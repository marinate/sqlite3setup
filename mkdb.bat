echo off
rem
rem make sample tables
rem
rem set CMD=-cmd "PRAGMA encoding = 'UTF-8'"

cd sqlite3
if Not Exist db (
    echo ---- Making Directory db ----
    mkdir db
)

if Exist db\mydb_org.db (
    echo del db\mydb_org.db
    del db\mydb_org.db
)
echo .\sqlite3.exe %CMD% db\mydb_org.db
.\sqlite3.exe %CMD% db\mydb_org.db ".read ..\\datas\\create_tables_s3.sql"
.\sqlite3.exe %CMD% db\mydb_org.db ".read ..\\datas\\datas_org_s3.sql"
.\sqlite3.exe %CMD% db\mydb_org.db ".read ..\\datas\\sql_sample_org_s3.sql"
.\sqlite3.exe %CMD% db\mydb_org.db ".read ..\\datas\\create_tables_hr_s3.sql"
.\sqlite3.exe %CMD% db\mydb_org.db ".read ..\\datas\\datas_hr_org_s3.sql"
.\sqlite3.exe %CMD% db\mydb_org.db ".read ..\\test\\fktest_s3.sql"
.\sqlite3.exe %CMD% db\mydb_org.db ".read ..\\test\\edu.sql"

if Exist db\mydb_1.db (
    echo del db\mydb_1.db
    del db\mydb_1.db
)
echo .\sqlite3.exe %CMD% db\mydb_1.db
.\sqlite3.exe %CMD% db\mydb_1.db ".read ..\\datas\\create_tables_s3.sql"
.\sqlite3.exe %CMD% db\mydb_1.db ".read ..\\datas\\datas_1_s3.sql"
.\sqlite3.exe %CMD% db\mydb_1.db ".read ..\\datas\\sql_sample_1_s3.sql"
.\sqlite3.exe %CMD% db\mydb_1.db ".read ..\\datas\\create_tables_hr_s3.sql"
.\sqlite3.exe %CMD% db\mydb_1.db ".read ..\\datas\\datas_hr_1_s3.sql"
.\sqlite3.exe %CMD% db\mydb_1.db ".read ..\\test\\fktest_1_s3.sql"
.\sqlite3.exe %CMD% db\mydb_1.db ".read ..\\test\\edu_1.sql"

if Exist db\mydb_2.db (
    echo del db\mydb_2.db
    del db\mydb_2.db
)
echo .\sqlite3.exe %CMD% db\mydb_2.db
.\sqlite3.exe %CMD% db\mydb_2.db ".read ..\\datas\\create_tables_s3.sql"
.\sqlite3.exe %CMD% db\mydb_2.db ".read ..\\datas\\datas_2_s3.sql"
.\sqlite3.exe %CMD% db\mydb_2.db ".read ..\\datas\\sql_sample_2_s3.sql"
.\sqlite3.exe %CMD% db\mydb_2.db ".read ..\\datas\\create_tables_hr_s3.sql"
.\sqlite3.exe %CMD% db\mydb_2.db ".read ..\\datas\\datas_hr_2_s3.sql"
.\sqlite3.exe %CMD% db\mydb_2.db ".read ..\\test\\fktest_2_s3.sql"
.\sqlite3.exe %CMD% db\mydb_2.db ".read ..\\test\\edu_2.sql"

if Exist db\mydb_3.db (
    echo del db\mydb_3.db
    del db\mydb_3.db
)
echo .\sqlite3.exe %CMD% db\mydb_3.db
.\sqlite3.exe %CMD% db\mydb_3.db ".read ..\\datas\\create_tables_s3.sql"
.\sqlite3.exe %CMD% db\mydb_3.db ".read ..\\datas\\datas_3_s3.sql"
.\sqlite3.exe %CMD% db\mydb_3.db ".read ..\\datas\\sql_sample_3_s3.sql"
.\sqlite3.exe %CMD% db\mydb_3.db ".read ..\\datas\\create_tables_hr_s3.sql"
.\sqlite3.exe %CMD% db\mydb_3.db ".read ..\\datas\\datas_hr_3_s3.sql"
.\sqlite3.exe %CMD% db\mydb_3.db ".read ..\\test\\fktest_3_s3.sql"
.\sqlite3.exe %CMD% db\mydb_3.db ".read ..\\test\\edu_3.sql"

copy ..\version.txt .

if Exist db\setupinfo.txt (
    echo del db\setupinfo.txt
    del db\setupinfo.txt
)
if Exist mydb.db (
    echo del mydb.db
    del mydb.db
)
if Exist chinook.db (
    echo del chinook.db
    del chinook.db
)

echo Finished.
