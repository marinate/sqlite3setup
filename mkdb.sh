#!/bin/sh
#
# make sample tables
#r
#rset CMD=-cmd "PRAGMA encoding = 'UTF-8'"
version='1.XX'
versiondate='2021/9/30'
versionfile=version.txt

if [ -f "$versionfile" ]; then
    version=`head -1 $versionfile | tr -d "\r"`
    versiondate=`tail -1 $versionfile | tr -d "\r"`
fi

if [ ! -d sqlite3 ]; then
    echo ---- Making sqlite3 dir -----
    mkdir sqlite3
fi
cd sqlite3
if [ ! -d db ]; then 
    echo ---- Making Directory db ----
    mkdir db
fi

for i in "org" "1" "2" "3"
do
    if [ -f db/mydb_$i.db ]; then 
        echo removing  db/mydb_${i}.db
        rm -f db/mydb_${i}.db
    fi
    echo sqlite3 $CMD db/mydb_$i.db
    sqlite3 $CMD db/mydb_${i}.db ".read ../datas/create_tables_s3.sql"
    sqlite3 $CMD db/mydb_${i}.db ".read ../datas/datas_${i}_s3.sql"
    sqlite3 $CMD db/mydb_${i}.db ".read ../datas/sql_sample_${i}_s3.sql"
    sqlite3 $CMD db/mydb_${i}.db ".read ../datas/create_tables_hr_s3.sql"
    sqlite3 $CMD db/mydb_${i}.db ".read ../datas/datas_hr_${i}_s3.sql"
    sqlite3 $CMD db/mydb_${i}.db ".read ../test/fktest_s3.sql"
    sqlite3 $CMD db/mydb_${i}.db ".read ../test/edu.sql"
    sqlite3 $CMD db/mydb_${i}.db "insert into version values('$version', '$versiondate')"
done
exit

if [ -f db/setupinfo.txt ]; then
    echo removing db/setupinfo.txt
    del db\setupinfo.txt
fi
if [ -f mydb.db ]; then
    echo removing mydb.db
    rm -f mydb.db
fi
if [ -f chinook.db ]; then
    echo removing chinook.db
    rm -f chinook.db
fi

echo Finished.
