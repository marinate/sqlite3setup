echo off
setlocal enabledelayedexpansion

set curdir=%~dp0

echo -- setup sqlite3
set DBNAME=mydb.db
set HOMEDIR=%USERPROFILE%
set mydbdir=.\db
set mydbfilename=''
set setuptxt=.\db\setupinfo.txt

if Exist %setuptxt% (
    for /f %%a in (%setuptxt%) do (
        set mydbfilename=%%a
    )
) else (
    set /a RND=%RANDOM%
    set /a "N=(RND %% 3) + 1"
    set mydbfilename=%mydbdir%\mydb_!N!.db
    echo !mydbfilename! > %setuptxt%
)
if Exist %DBNAME% del %DBNAME% (
    copy %mydbfilename% %DBNAME%
)
if Exist db\chinook.db (
    echo copy db\chinook.db .
    copy db\chinook.db .
)
set sqliterc=%HOMEDIR%\.sqliterc
if Not Exist %sqliterc% (
    rem echo COPY .sqliterc
    rem COPY .sqliterc %HOMEDIR%
)

echo -- Finished.---
pause
