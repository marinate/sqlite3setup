#!/bin/sh

curdir=`pwd`

echo -- setup sqlite3
DBNAME=mydb.db
HOMEDIR=$HOME
mydbdir=./db
mydbfilename=''
setuptxt=./db/setupinfo.txt

echo set $setuptxt
if [ -f "$setuptxt" ]; then
    mydbfilename=`cat $setuptxt`
else
    R=$RANDOM
    RND=$((R % 3))
    N=$((RND+1))
    mydbfilename=$mydbdir/mydb_${N}.db
    echo $mydbfilename > $setuptxt
fi

if [ -f "$DBNAME" ]; then
    rm -f $DBNAME
fi
cp $mydbfilename $DBNAME
if [ -f db/chinook.db ]; then
    echo cp db/chinook.db .
    cp -a db/chinook.db .
fi
sqliterc=$HOMEDIR/.sqliterc
if [ ! -f "$sqliterc" ]; then
    echo cp .sqliterc $HOMDDIR/
    cp -a .sqliterc $HOMEDIR/
fi


echo -- Finished.---
