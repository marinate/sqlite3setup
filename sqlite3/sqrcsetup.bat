echo off
setlocal enabledelayedexpansion

set curdir=%~dp0

echo -- setup sqlite3
set DBNAME=mydb.db
set HOMEDIR=%USERPROFILE%
set mydbdir=.\db
set mydbfilename=''
set setuptxt=.\db\setupinfo.txt


set sqliterc=%HOMEDIR%\.sqliterc
if Not Exist %sqliterc% (
    echo COPY .sqliterc
    COPY .sqliterc %HOMEDIR%
) else (
    echo do nothing.
)

echo -- Finished.---
pause
