#!/bin/sh
curdir=`pwd`

echo -- setup sqlite3
DBNAME=mydb.db
HOMEDIR=$HOME
mydbdir=./db
mydbfilename=''
setuptxt=./db/setupinfo.txt


sqliterc=$HOMEDIR/.sqliterc
if [ ! -f $sqliterc ]; then
    echo CP .sqliterc
    cp -a .sqliterc $HOMEDIR/
else
    echo do nothing.
fi

echo -- Finished.---
